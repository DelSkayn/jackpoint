rm -rf ./dist
(cd jackpoint && npm update)
(cd jackpoint && npm run build)
mkdir dist
mkdir dist/front
cp -r jackpoint/dist/* dist/front/
cp .env dist/
cp Rocket.toml dist/
cargo build --release
cp target/release/jackpoint dist

