import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Intro from '@/components/Intro'
import Register from '@/components/Register'
import Posts from '@/components/Posts'
import NewPost from '@/components/NewPost'
import FullPost from '@/components/FullPost'

Vue.use(Router)

export default new Router({
    mode: 'history',
    scrollBehavior: function (to, from, savedPosition) {
        if (to.hash) {
            return {selector: to.hash}
        } else {
            return {x: 0, y: 0}
        }
    },
    routes: [
        {
            path: '/posts/:id',
            props: true,
            name: 'FullPost',
            component: FullPost
        },
        {
            path: '/new-post',
            name: 'NewPost',
            component: NewPost
        },
        {
            path: '/posts',
            name: 'Posts',
            component: Posts
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/login',
            name: 'Hello',
            component: Hello
        },
        {
            path: '/',
            name: 'Intro',
            component: Intro
        }
    ]
})
