-- Your SQL goes here
CREATE TABLE posts (
    id BIGSERIAL PRIMARY KEY,
    title TEXT NOT NULL,
    text TEXT NOT NULL,
    tag TEXT,
    timestamp TIMESTAMP DEFAULT current_timestamp NOT NULL,
    poster BIGINT NOT NULL,
    FOREIGN KEY (poster) REFERENCES users(id)
);

