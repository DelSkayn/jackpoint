-- Your SQL goes here

CREATE TABLE comments(
    id BIGSERIAL PRIMARY KEY,
    response BIGINT,
    text TEXT NOT NULL,
    poster BIGINT NOT NULL,
    post BIGINT NOT NULL,
    timestamp TIMESTAMP DEFAULT current_timestamp NOT NULL,
    FOREIGN KEY (poster) REFERENCES users(id),
    FOREIGN KEY (post) REFERENCES posts(id)
);
