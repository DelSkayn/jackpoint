use diesel::prelude::*;
use diesel;
use db::DbConnections;
use session::Session;
use db::schema::{comments,posts,users};
use siphasher::sip::SipHasher;
use rand;
use std::mem;
use std::hash::{Hash,Hasher};

use rocket_contrib::Json;
use rocket::State;
use rocket::http::{Cookies,Cookie};

#[derive(Serialize,Deserialize,Debug)]
struct PostCommentRequest{
    text: String,
    response: Option<i64>,
}

#[derive(Serialize,Deserialize)]
struct PostCommentResponse{
    successfull: bool,
    reason: Option<String>,
    commenter: Option<String>,
    id: Option<i64>,
}

#[derive(Serialize,Deserialize)]
pub struct Comment{
    id: i64,
    response: Option<i64>,
    text: String,
    poster: String,
}

#[derive(Queryable)]
pub struct TempComment{
    id: i64,
    response: Option<i64>,
    text: String,
    poster: Option<String>,
}

impl TempComment{
    fn into_comment(self) -> Comment{
        Comment{
            id: self.id,
            response: self.response,
            text: self.text,
            poster: self.poster.unwrap()
        }
    }
}

#[derive(Serialize,Deserialize)]
struct CommentsResponse{
    comments: Vec<Comment>
}

#[derive(Insertable)]
#[table_name="comments"]
pub struct NewComment<'a>{
    text: &'a str,
    poster: i64,
    post: i64,
    response: Option<i64>,
}

#[post("/api/comment/<id>", format = "application/json", data = "<comment>")]
fn post_comment(id: i64, comment: Json<PostCommentRequest>,db: State<DbConnections>,session: State<Session>,mut cookies: Cookies) -> Json<PostCommentResponse>{
    if let Some(x) = session.get_cookie(&mut cookies){
        let db = db.borrow();
        let total_posts: i64 = posts::table.filter(posts::id.eq(&id)).count().get_result(&*db).unwrap();
        if total_posts == 0{
            return Json(PostCommentResponse{
                successfull: false,
                reason: Some("Post does not exists".to_string()),
                commenter: None,
                id: None
            });
        }
        let user_id = users::table.filter(users::name.eq(&x.username))
            .select(users::id)
            .first(&*db)
            .unwrap();

        let new_id: i64;

        {
            let new_comment = NewComment{
                text: &comment.text,
                poster: user_id,
                post: id,
                response: comment.response,
            };

            new_id = diesel::insert(&new_comment)
                .into(comments::table)
                .returning(comments::id)
                .get_result(&*db)
                .unwrap();
        }

        return Json(PostCommentResponse{
            successfull: true,
            reason: None,
            commenter: Some(x.username),
            id: Some(new_id)
        })
    }
    Json(PostCommentResponse{
        successfull: false,
        reason: Some("Not logged in".to_string()),
        commenter: None,
        id: None
    })

}

#[get("/api/comment/<id>")]
fn get_comment(id: i64, db: State<DbConnections>) -> Json<CommentsResponse>{
    let db = db.borrow();
    let mut temp: Vec<TempComment> = comments::table.filter(comments::post.eq(id))
        .left_join(
            users::table.on(
                users::id.eq(comments::poster)
            )
        ).select((comments::id,comments::response,comments::text,users::name.nullable()))
        .order(comments::timestamp.asc())
        .load(&*db).unwrap();

    let res = temp.drain(..).map(TempComment::into_comment).collect();

    Json(CommentsResponse{
        comments: res,
    })
}
