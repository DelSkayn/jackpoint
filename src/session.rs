
use rand;

use std::sync::{Arc, RwLock};
use std::sync::atomic::{Ordering, AtomicUsize};
use std::collections::HashMap;

use rocket::http::{Cookies,Cookie};

#[derive(Clone)]
pub struct SessionData{
    pub username: String,
    pub session_id: u64,
}

pub struct Session {
    data: RwLock<HashMap<u64, SessionData>>,
}

impl Session {
    pub fn new() -> Self {
        Session {
            data: RwLock::new(HashMap::new()),
        }
    }

    pub fn add(&self,username: String) -> u64{
        let id = rand::random();
        self.data.write().unwrap().insert(id,SessionData{
            username: username,
            session_id: id,
        });
        id
    }

    pub fn remove(&self,session_id: u64){
        self.data.write().unwrap().remove(&session_id);
    }

    pub fn remove_cookie(&self,cookies: &mut Cookies) -> bool{
        if let Some(cookie) = cookies.get_private("session"){
            if let Ok(x) = cookie.value().parse(){
                return self.data.write().unwrap().remove(&x).is_some();
            }
        }
        false
    }

    pub fn get_cookie(&self,cookies: &mut Cookies) -> Option<SessionData>{
        if let Some(cookie) = cookies.get_private("session"){
            if let Ok(x) = cookie.value().parse(){
                return self.data.read().unwrap().get(&x).cloned()
            }
        }
        None
    }

    pub fn is_valid(&self,session_id: u64) -> bool{
        self.data.read().unwrap().contains_key(&session_id)
    }

    pub fn is_valid_cookie(&self,cookies: &mut Cookies) -> bool{
        if let Some(cookie) = cookies.get_private("session"){
            if let Ok(x) = cookie.value().parse(){
                if self.is_valid(x) {
                    return true;
                }
            }
        }
        false
    }

    pub fn add_cookie(&self,username: String,cookies: &mut Cookies){
            let ses_id = self.add(username);
            cookies.add_private(Cookie::build("session",ses_id.to_string()).permanent().finish());
    }

}
