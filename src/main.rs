#![feature(plugin)]
#![plugin(rocket_codegen)]
#![allow(dead_code)]
#![allow(unused_imports)]

extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

extern crate serde;
#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;
extern crate dotenv;
extern crate fern;
#[macro_use]
extern crate log;
extern crate chrono;
extern crate crossbeam;
extern crate siphasher;
extern crate rand;

mod session;
mod user;
mod posts;
mod comment;

pub mod db;

use self::rocket::response;
use std::path::{PathBuf,Path};

#[get("/static/<path..>")]
fn stat(path: PathBuf) -> Option<response::NamedFile>{
    response::NamedFile::open(Path::new("front/static/").join(path)).ok()
}

#[get("/<_path..>", rank = 99)]
fn catch_all(_path: PathBuf) -> response::NamedFile{
    response::NamedFile::open("front/index.html").unwrap()
}

#[get("/")]
fn index() -> response::NamedFile{
    response::NamedFile::open("front/index.html").unwrap()
}

fn main(){
    if cfg!(debug_assertions) {
        fern::Dispatch::new()
            .format(|out, message, record| {
                out.finish(format_args!("{}[{}][{}] {}",
                    chrono::Local::now()
                    .format("[%Y-%m-%d][%H:%M:%S]"),
                    record.target(),
                    record.level(),
                    message))
            })
        .level(log::LogLevelFilter::Debug)
            .chain(std::io::stdout())
            .apply().unwrap();
    }

    dotenv::dotenv().map_err(|e|{
        warn!("Error while trying to read environment file: {:?}",e);
    }).ok();

    let connections = db::DbConnections::new().unwrap();

    let server = rocket::Rocket::ignite()
        .manage(session::Session::new())
        .manage(connections)
        .mount("/", routes!
               [ user::login 
               , user::register
               , user::get_login 
               , user::get_logout
               , comment::post_comment
               , comment::get_comment
               , posts::posts 
               , posts::specific_posts 
               , posts::postposts ]);

    println!("{}",cfg!(debug_assertions));
    if cfg!(debug_assertions){
        server.launch();
    }else{
        server.mount("/",routes![ stat
                     , index
                     , catch_all ]).launch();
    }
}
