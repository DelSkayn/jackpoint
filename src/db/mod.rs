
use diesel::prelude::*;
use diesel::pg;
use diesel::result;
use crossbeam::sync::MsQueue;

use std::{env,fmt,error};
use std::ops::{Deref,DerefMut};

pub mod schema;
pub mod models;
use self::schema::*;

#[derive(Debug)]
pub enum Error{
    NoUrl,
    ConnectionError(result::ConnectionError),
}

impl fmt::Display for Error{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::NoUrl => write!(f,"No database url was specified"),
            Error::ConnectionError(ref e) => write!(f,"ConnectionError: {}",e),
        }
    }
}

impl From<result::ConnectionError> for Error{
    fn from(e: ConnectionError) -> Self{
        Error::ConnectionError(e)
    }
}

impl error::Error for Error{
    fn description(&self) -> &str{
        match *self{
            Error::NoUrl => "No database url was specified\
                Please make sure that the environment var DATABASE_URL is set, either via file or normal",
            Error::ConnectionError(ref e)  => e.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error>{
        match *self{
            Error::NoUrl => None,
            Error::ConnectionError(ref e)  => Some(e),
        }
    }
}

pub struct DbConnections{
    connections: MsQueue<pg::PgConnection>,
}

impl DbConnections{
    pub fn new() -> Result<Self,Error>{
        info!("Establishing database connections");
        let database_url = env::var("DATABASE_URL").map_err(|_|{
            Error::NoUrl
        })?;

        let num_connections = env::var("DATABASE_CONNECTIONS")
            .ok()
            .and_then(|e|{
                e.parse().ok()
            }).unwrap_or_else(||{
                warn!("No amount of connections specified. using default of 4");
                4
            });

        let connections = MsQueue::new();
        for i in 0..num_connections{
            debug!("Creating database connection {}, to {}",i,database_url);
            connections.push(pg::PgConnection::establish(&database_url)?);
        }
        Ok(DbConnections{
            connections,
        })
    }

    pub fn borrow(&self) -> DbCon{
        DbCon{
            connection: Some(self.connections.pop()),
            borrow: self
        }
    }
}

pub struct DbCon<'a>{
    connection: Option<pg::PgConnection>,
    borrow: &'a DbConnections,
}

impl<'a> Deref for DbCon<'a>{
    type Target = pg::PgConnection;
    fn deref(&self) -> &Self::Target{
        self.connection.as_ref().unwrap()
    }
}

impl<'a> DerefMut for DbCon<'a>{
    fn deref_mut(&mut self) -> &mut pg::PgConnection{
        self.connection.as_mut().unwrap()
    }
}

impl<'a> Drop for DbCon<'a>{
    fn drop(&mut self){
        self.borrow.connections.push(self.connection.take().unwrap())
    }
}
