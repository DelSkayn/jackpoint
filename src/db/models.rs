
use super::schema::{posts,users,comments};

#[derive(Identifiable,Queryable)]
pub struct User{
    pub id: i64,
    pub name: String,
    pub hash: i64,
    pub salt: i64,
}

#[derive(Identifiable,Queryable,Associations)]
#[belongs_to(User,foreign_key="poster")]
pub struct Post{
    id: i64,
    title: String,
    text: String,
    poster: i64,
}

#[derive(Identifiable,Queryable,Associations)]
#[belongs_to(User,foreign_key="poster")]
#[belongs_to(Post,foreign_key="post")]
pub struct Comment{
    id: i64,
    text: String,
    poster: i64,
    post: i64,
}
