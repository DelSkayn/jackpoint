
use diesel::prelude::*;
use diesel::expression::dsl;
use diesel::types;
use diesel;
use db::{DbConnections,DbCon};
use db::schema::{posts,users,comments};
use db::models::{User};

use rocket_contrib::Json;
use rocket::http::Cookies;
use rocket::State;
use session::Session;

#[derive(Serialize,Deserialize)]
pub struct PostResponse{
    posts: Vec<TitlePost>,
}

#[derive(Serialize,Deserialize)]
pub struct TitlePost{
    id: i64,
    title: String,
    tag: Option<String>,
    poster: String,
}

#[derive(Serialize,Deserialize)]
pub struct CompletePost{
    id: i64,
    title: String,
    text: String,
    tag: Option<String>,
    poster: String,
}

#[derive(Serialize,Deserialize)]
pub struct PostRequest{
    title: String,
    tag: Option<String>,
    text: String,
}

#[derive(Serialize,Deserialize)]
pub struct PostPostResponse{
    successfull: bool,
    reason: Option<String>,
}

#[derive(Insertable)]
#[table_name="posts"]
pub struct NewPost<'a>{
    pub title: &'a str,
    pub text: &'a str,
    pub tag: Option<&'a str>,
    pub poster: i64,
}


#[derive(Queryable)]
pub struct Post{
    id: i64,
    title: String,
    tag: Option<String>,
    name: Option<String>
}

#[derive(Queryable)]
pub struct FullPost{
    id: i64,
    title: String,
    text: String,
    tag: Option<String>,
    name: Option<String>,
}

#[get("/api/posts")]
fn posts(db: State<DbConnections>) -> Json<PostResponse>{
    let db = db.borrow();
    let mut posts: Vec<Post> = posts::table
        .left_join(
            users::table.on(
                users::id.eq(posts::poster)
            )
        ).left_join(
            comments::table.on(
                comments::post.eq(posts::id)
            )
        )
        .group_by((posts::id,users::name))
        .order(dsl::sql::<types::Bool>("COALESCE(GREATEST(posts.timestamp, MAX(comments.timestamp)), posts.timestamp) DESC"))
        .select((posts::id,posts::title,posts::tag,users::name.nullable()))
        .load(&*db).unwrap(); 

    let res = posts.drain(..).map(|e|{
            TitlePost{
                id: e.id,
                title: e.title,
                tag: e.tag,
                poster: e.name.unwrap()
            }
        }).collect();

    Json(PostResponse{
        posts: res
    })
}

#[get("/api/post/<id>")]
fn specific_posts(id: i64,db: State<DbConnections>) -> Option<Json<CompletePost>>{
    let db = db.borrow();
    let posts: QueryResult<FullPost> = posts::table
        .filter(posts::id.eq(id))
        .left_join(
            users::table.on(
                users::id.eq(posts::poster)
            )
        ).select((posts::id,posts::title,posts::text,posts::tag,users::name.nullable()))
        .first(&*db); 

    let posts = match posts{
        Ok(x) => x,
        Err(_) => return None,
    };

    Some(Json(CompletePost{
        id: posts.id,
        title: posts.title,
        tag: posts.tag,
        text: posts.text,
        poster: posts.name.unwrap()
    }))

}

fn get_user_id(username: &str,db: &DbCon) -> i64{
    use db::schema::users::dsl::*;

    users.filter(name.eq(username)).select(id).load(&**db).unwrap()[0]
}

#[post("/api/post", format = "application/json", data = "<post>")]
fn postposts(post: Json<PostRequest>,session: State<Session>,db: State<DbConnections>,mut cookies: Cookies) -> Json<PostPostResponse>{
    use db::schema::posts::dsl::*;
    if let Some(x) = session.get_cookie(&mut cookies){
        let db = db.borrow();
        let user = get_user_id(&x.username,&db);

        let new_post = NewPost{
            title: &post.title,
            text: &post.text,
            tag: post.tag.as_ref().map(|x| x.as_str()),
            poster: user,
        };

        diesel::insert(&new_post)
            .into(posts)
            .execute(&*db)
            .expect("Error creating new post");
        return Json(PostPostResponse{
            successfull: true,
            reason: None
        })
    }
    return Json(PostPostResponse{
        successfull: false,
        reason: Some("Not logged in".to_string())
    })


}
