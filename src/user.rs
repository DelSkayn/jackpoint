
use diesel::prelude::*;
use diesel;
use db::DbConnections;
use session::Session;
use db::schema::users;
use db::models::User;
use siphasher::sip::SipHasher;
use rand;
use std::mem;
use std::hash::{Hash,Hasher};

use rocket_contrib::Json;
use rocket::State;
use rocket::http::{Cookies,Cookie};

#[derive(Serialize,Deserialize,Debug)]
struct RegisterRequest{
    username: String,
    password: String,
}

#[derive(Serialize,Deserialize)]
struct RegisterResponse{
    successfull: bool,
    reason: Option<String>,
}

#[derive(Serialize,Deserialize,Debug)]
struct LoginRequest{
    username: String,
    password: String,
}

#[derive(Serialize,Deserialize)]
struct LoginResponse{
    successfull: bool,
    reason: Option<String>,
}

#[post("/api/login", format = "application/json", data = "<user>")]
fn login(user: Json<LoginRequest>,db: State<DbConnections>,session: State<Session>,mut cookies: Cookies) -> Json<LoginResponse>{
    use db::schema::users::dsl::*;

    info!("Recieved login request: {:?}",user);
    if session.is_valid_cookie(&mut cookies){
        return Json(LoginResponse{
            successfull: true,
            reason: None,
        });
    }
    let db = db.borrow();

    if let Some(present_user) = users.filter(name.eq(&user.username))
        .load::<User>(&*db)
        .unwrap()
        .pop()
    {
        let mut hasher = SipHasher::new();
        present_user.salt.hash(&mut hasher);
        user.password.hash(&mut hasher);
        let login_hash: i64 = unsafe{
            mem::transmute(hasher.finish())
        };
        if present_user.hash == login_hash {
            session.add_cookie(user.username.clone(),&mut cookies);
            return Json(LoginResponse{
                successfull: true,
                reason: None,
            })
        }
    }

    Json(LoginResponse{
        successfull: false,
        reason: Some("Username or password invalid".to_string()),
    })
}

#[get("/api/login")]
fn get_login(session: State<Session>,mut cookies: Cookies) -> Json<bool>{
    Json(session.is_valid_cookie(&mut cookies))
}

#[get("/api/logout")]
fn get_logout(session: State<Session>,mut cookies: Cookies) -> Json<LoginResponse>{
    if session.remove_cookie(&mut cookies){
        return Json(LoginResponse{
            successfull: true,
            reason: None
        })
    }
    Json(LoginResponse{
        successfull: false,
        reason: Some("Not logged in".to_string()), 
    })
}

#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser<'a> {
    pub name: &'a str,
    pub salt: i64,
    pub hash: i64,
}

#[post("/api/register", format = "application/json", data = "<user>")]
fn register(user: Json<RegisterRequest>,db: State<DbConnections>) -> Json<RegisterResponse>{
    use db::schema::users::dsl::*;

    println!("Recieved register request: {:?}",user);
    let db = db.borrow();
    let users_using_name: i64 = users.filter(name.eq(&user.username)).count().get_result(&*db).unwrap();
    if  users_using_name != 0 {
        return Json(RegisterResponse{
            successfull: false,
            reason: Some("Username already exists".to_string()),
        });
    }

    let mut hasher = SipHasher::new();
    let new_salt: i64 = rand::random();
    new_salt.hash(&mut hasher);
    user.password.hash(&mut hasher);
    let new_hash: i64 = unsafe{
        mem::transmute(hasher.finish())
    };

    let new_user = NewUser{
        name: &user.username,
        salt: new_salt,
        hash: new_hash,
    };

    diesel::insert(&new_user)
        .into(users)
        .execute(&*db)
        .expect("Error creating new user");

    Json(RegisterResponse{
        successfull: true,
        reason: None,
    })
}
